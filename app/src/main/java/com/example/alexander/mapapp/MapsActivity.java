package com.example.alexander.mapapp;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.database.Cursor;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;                                         //The Map
    private Marker tapMarker;                                       //Marker to be used for tapping locations to add
    public LatLng currentTappedPosition = new LatLng(-34, 151);     //Stores current position when map is tapped - sets default at Sydney
    DBManager dbManager;                                            //An instance of the Database Manager created so we can interact with the Database

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        dbManager = new DBManager(this, null, null, 1);

        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /**
         * You will need to add functionality so that the FAB will add a location to the database
         */
        //Floating Action Button - when a user presses this, store the current location/position stored by the MapClickListener
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Default action to show you that the FAB is pressed
                Snackbar.make(view, "Put the Lat and Lng in here", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                displayLocations();
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        /**
         * Add an onMapClickListener here
         *
         * When the user taps the map, change the location of the 'tapMarker' marker
         * This location is the location that will be stored when the user pressed the '+' FAB
         */

        displayLocations();
    }

    /**
     * Displays all stored Locations in the database as Markers.
     *
     * Calls the function getAllItems
     * You will need to iterate through the cursor to display all markers after that
     * During the iteration, it will get the column information and plug it into a temp Latitude and Longitude
     * Finally using the LatLng it will set a marker at the location
     */
    public void displayLocations()
    {
        mMap.clear();
        Cursor cursor = dbManager.getAllItems();

        LatLng tempPosition;
        double tempLat, tempLng;

        /**
         * Add code for iterating through the cursor here
         */

        //This Marker is the marker used to indicate where a user has tapped on the map - you will need to move it with a MapClickListener
        tapMarker = mMap.addMarker(new MarkerOptions().position(currentTappedPosition).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tapMarker.getPosition()));
    }
}
